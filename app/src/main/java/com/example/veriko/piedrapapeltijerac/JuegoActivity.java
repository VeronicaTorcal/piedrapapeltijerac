package com.example.veriko.piedrapapeltijerac;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class JuegoActivity extends AppCompatActivity {

    TextView tiradas, tiradaNumero;
    TextView textoNombreJug;
    ListView botonesJuego;
    int tiradasQueLlevamos;
    int puntuacionJugador;
    int puntuacionMaquina;
    TextView puntuacionJug;
    TextView puntuacionMaq;
    ImageView jugadaJugador;
    ImageView jugadaMaquina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);
        tiradas=(TextView)findViewById(R.id.tiradas);
        tiradaNumero=(TextView)findViewById(R.id.tiradaNumero);
        textoNombreJug=(TextView)findViewById(R.id.textoNombreJug);
        puntuacionJug=(TextView)findViewById(R.id.puntuacionJug);
        puntuacionMaq=(TextView)findViewById(R.id.puntuacionMaq);
        jugadaJugador=(ImageView)findViewById(R.id.jugadaJugador);
        jugadaMaquina=(ImageView)findViewById(R.id.jugadaMaquina);

        // tiradas.setText(datoTiradas);
        SharedPreferences ajustes = getSharedPreferences("configuracion", MODE_PRIVATE);
        final String nombreJ=ajustes.getString("nombreJ", "jugador");
        final int tiradasDef=ajustes.getInt("tiradasDef", 3);
        // VARIABLE DE NUMERO DE TIRADAS QUE LLEVAMOS, QUE IRÁ AUMENTANDO AL PASAR POR UN WHILE
        tiradasQueLlevamos=1;
        puntuacionJugador=0;
        puntuacionMaquina=0;


        tiradas.setText(String.valueOf(tiradasDef));
        textoNombreJug.setText("Puntos "+String.valueOf(nombreJ));
        botonesJuego=(ListView)findViewById(R.id.botonesJuego);
        // tiradaNumero.setText(String.valueOf(tiradasQueLlevamos));

        botonesJuego.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //IGUAL AQUI HABRÍA QUE CREAR UN ALEATORIO PARA PODER COMPARARLO AL COMPARAR CON POSITION
                    if (tiradasQueLlevamos<=tiradasDef){
                        tiradaNumero.setText(String.valueOf(tiradasQueLlevamos));
                        int aleatorio = (int) (Math.random() * 3);

                        Context context = getApplicationContext();
//                        String texto = "Toast mágico.";
                        //Toast toast = Toast.makeText(context, texto+" puntos mios: "+puntuacionJugador+" puntos maquina: "+puntuacionMaquina, Toast.LENGTH_LONG);
//                        toast.show();

                        switch (position){
                            case 0:
//                        lo que hace si pulsas piedra
                                jugadaJugador.setImageResource(R.drawable.piedra);
                                if (aleatorio==0){
                                    // empate, no se suma puntos
                                    Toast toast =Toast.makeText(context, "empate "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.piedra);
                                }else if (aleatorio==1){
                                    puntuacionMaquina++;
                                    Toast toast =Toast.makeText(context, "derrota "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.papel);

                                }else {
                                    puntuacionJugador++;
                                    Toast toast =Toast.makeText(context, "victoria "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.tijera);

                                }
                                break;
                            case 1:
//                        lo que hace si pulsas papel
                                jugadaJugador.setImageResource(R.drawable.papel);
                                if (aleatorio==0){
                                    puntuacionJugador++;
                                    Toast toast =Toast.makeText(context, "victoria"+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.piedra);
                                }else if (aleatorio==1){
                                    Toast toast =Toast.makeText(context, "empate"+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.papel);
                                    // empate, no se suma puntos
                                }else {
                                    puntuacionMaquina++;
                                    Toast toast =Toast.makeText(context, "derrota "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.tijera);
                                }
                                break;
                            case 2:
//                        lo que hace si pulsas tijera
                                jugadaJugador.setImageResource(R.drawable.tijera);
                                if (aleatorio==0){
                                    puntuacionMaquina++;
                                    Toast toast =Toast.makeText(context, "derrota "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.piedra);
                                }else if (aleatorio==1){
                                    puntuacionJugador++;
                                    Toast toast =Toast.makeText(context, "victoria "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.papel);
                                }else {
                                    Toast toast =Toast.makeText(context, "empate "+ "puntos tuyos"+puntuacionJugador+" puntos maquina "+puntuacionMaquina, Toast.LENGTH_LONG);
                                    toast.show();
                                    jugadaMaquina.setImageResource(R.drawable.tijera);
                                    // empate, no se suma puntos
                                }
                                break;
                        }
                        tiradasQueLlevamos++;
                        // sin que haga falta darle a ningun boton, si ya has echado todas las partidas entra en el if, si no no
                        if (tiradasQueLlevamos==(tiradasDef+1)){
                            Intent i=new Intent(getApplicationContext(),ResultadoActivity.class);
                            Bundle datos=new Bundle();
                            datos.putInt("PUNTOSJ", puntuacionJugador);
                            datos.putInt("PUNTOSM", puntuacionMaquina);
                            i.putExtras(datos);
                            startActivity(i);
                        } // fin bucle if

//                        poner aqui

                        puntuacionJug.setText(String.valueOf(puntuacionJugador));
                        puntuacionMaq.setText(String.valueOf(puntuacionMaquina));

                    }// fin bucle if.

                }

            });





    }
}
