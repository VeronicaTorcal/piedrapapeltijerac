package com.example.veriko.piedrapapeltijerac;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista=(ListView)findViewById(R.id.lista);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent;
                switch (position){
                    case 0:
                        intent = new Intent(getApplicationContext(), JuegoActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        //Con el fin de empezar a mostrar una nueva actividad lo que necesitamos es un intent
                        intent = new Intent(getApplicationContext(),AjustesActivity.class);

                        // Aquí pasaremos el parámetro de la intención creada previamente
                        startActivity(intent);


                        break;
                    case 2:
                        Context context = getApplicationContext();
                        String texto = "Esta aplicación está hecha por mi, va a salir todo bien por mis cojones, y punto";
                        Toast toast = Toast.makeText(context, texto, Toast.LENGTH_SHORT);
                        toast.show();
                        break;

                    case 3:
                        Intent salida=new Intent( Intent.ACTION_MAIN); //Llamando a la activity principal
                        finish(); // La cerramos.
                        break;


                }

            }
        });
    }
}
