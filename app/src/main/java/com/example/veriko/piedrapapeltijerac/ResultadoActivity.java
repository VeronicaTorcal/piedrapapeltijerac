package com.example.veriko.piedrapapeltijerac;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ResultadoActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imagenResultado;
    TextView textoResultado;
    Button botonInicio;
    int puntosJ=0;
    int puntosM=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        imagenResultado=(ImageView)findViewById(R.id.imagenResultado);
        textoResultado=(TextView)findViewById(R.id.textoResultado);
        botonInicio=(Button)findViewById(R.id.botonInicio);

        Bundle datos=getIntent().getExtras();
        puntosJ=datos.getInt("PUNTOSJ");
        puntosM=datos.getInt("PUNTOSM");

        if (puntosJ>puntosM){
            imagenResultado.setImageResource(R.drawable.ganar);
            textoResultado.setText("HAS GANADO!! MOLA!!");
        }else if (puntosM==puntosJ){
            imagenResultado.setImageResource(R.drawable.empatar);
            textoResultado.setText("HAS EMPATADO!! PAL CASO... DERROTA");
        }else {
            imagenResultado.setImageResource(R.drawable.perder);
            textoResultado.setText("HAS PERDIDO!!! BUUUUHHHH!!!");
        }

        botonInicio.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);

    }
}
