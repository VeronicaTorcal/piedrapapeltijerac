package com.example.veriko.piedrapapeltijerac;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AjustesActivity extends AppCompatActivity {

    EditText nombre, tiradas;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);

        nombre=(EditText)findViewById(R.id.etnombre);
        tiradas=(EditText)findViewById(R.id.ettiradas);
        boton=(Button)findViewById(R.id.botonGuardar);

//        boton.setOnClickListener(this);
        final SharedPreferences ajustes=getSharedPreferences("configuracion", MODE_PRIVATE);
        // le pongo algo por defecto si no ponen su nombre. nombreJ es la key, jugador el valor x defecto
        final String nombreJ=ajustes.getString("nombreJ", "jugador");
        nombre.setText(String.valueOf(nombreJ));
        // lo mismo con las tiradas
        int tiradasDef=ajustes.getInt("tiradasDef", 3);
        tiradas.setText(String.valueOf(tiradasDef));

        boton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Editable stringNumTiradas = tiradas.getText();
                // referenciamos el objeto editor
                SharedPreferences.Editor editor=ajustes.edit();
                // agrego un ajuste
                editor.putString("nombreJ", nombre.getText().toString());
                editor.putInt("tiradasDef", Integer.parseInt(tiradas.getText().toString()));
                // ejecutar los ajustes
                editor.commit();
                // despues de guardar, que al darle al boton pase al main
//                Intent i = new Intent(getApplicationContext(),MainActivity.class);
////                // Aquí pasaremos el parámetro de la intención creada previamente
//                startActivity(i);
                // INTENTO DE LLEVAR EL NOMBRE Y LAS TIRADAS AL JUEGOACTIVITY
//                Intent i = new Intent(AjustesActivity.this, JuegoActivity.class);
//                String textoTiradas = tiradas.getText().toString();
//                i.putExtra("llaveTiradas", stringNumTiradas);
                //startActivity(i);
               // i.putExtra("nombreJ", nombre.getText());
               // i.putExtra("tiradasDef", stringNumTiradas);
                finish();
            }
        });
    }


}
